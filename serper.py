#!/usr/bin/python

import tornado.web
import tornado.websocket
import tornado.ioloop
from tele_url import TelegramBot
import json

class WebSocketHandler(tornado.websocket.WebSocketHandler):
        def open(self):
                print "New client connected"
                self.write_message("You are connected")

        def on_message(self, message):
                try:
                    data = message.split('|')
                    print data[0]
                    if data[0]=="send":
                        token = data[1]
                        chat_id = data[2]
                        msg = data[3]
                        self.write_message("send telegram")
                        tele = TelegramBot(token=token)
                        tele.send_message(msg=msg,chat_id=chat_id)
                        self.write_message("data has been send")
                    elif data[0]=="read":
                        token = data[1] 
                        tele = TelegramBot(token=token)
                        replay = tele.read_message()
                        print replay
                        self.write_message(replay)
                    else:
                        self.write_message(data[1])
                    print e
                except Exception as e:
                    self.write_message("error")

                #self.write_message(message)

        def on_close(self):
                print "Client disconnected"


application = tornado.web.Application([
    (r"/", WebSocketHandler),
])

if __name__ == "__main__":
    application.listen(3000)
    tornado.ioloop.IOLoop.instance().start()